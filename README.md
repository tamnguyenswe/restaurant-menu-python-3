*This is one part of the smart restaurant project, which controls the activities on client side (tables in restaurant)*

The server side controling part:
https://gitlab.com/presariohg/restaurant-server-side

**TODO**: 
+ move read buttons inside show qr out if needed when showing on 2 screens
+ choose one coding pattern to clean this mess up, MVC, MVVM, whatever, menubar has over 800 lines now
+ fix write log at server, take local time instead of gmt time

# Documentation / Manual
You can find the manual [here](https://drive.google.com/file/d/1jU0bD2fhAyh4zzXR8TzrtsNqmVUe9p72/view?usp=sharing) (Vietnamese)

### Dependencies
+ PCD8544 library for python 3: https://gitlab.com/presariohg/pcd8544-python3  
+ Python version >= 3.5

+ gpiozero:
```bash
    sudo pip3 install gpiozero
```

+ pyqrcode:
```bash
    sudo pip3 install pyqrcode
```

+ paho-mqtt:
```bash
    sudo pip3 install paho-mqtt
```