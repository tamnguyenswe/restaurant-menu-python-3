from MenuBar                import MenuBar
from gpiozero               import Button

import paho.mqtt.client
import threading

from pcd8544.lcd            import  LCD, ON, OFF
from pcd8544.fonts.font_vi  import  font_vi
from global_variables       import  TOPIC_NEW_MENU, SERVER_ADDRESS, TOPIC_ITEMS_RECEIVED, TABLE_ID, TOPIC_RECEIPT
# from pcd8544.fonts.font5x7  import  font5x7

import json 
import codecs

def button_init():

    BUTTON_1        =   Button(4)
    BUTTON_2        =   Button(17)
    BUTTON_3        =   Button(5)
    BUTTON_4        =   Button(6)

    global buttons

    buttons         =   [BUTTON_1, BUTTON_2, BUTTON_3, BUTTON_4]

def lcd_init():
    SCLK = 14 
    DIN  = 12 
    DC   = 4 
    CS   = 10 
    RST  = 5 
    BL   = 30

    PIN_OUT     =   {  
                    'SCLK'  :   SCLK,
                    'DIN'   :   DIN,
                    'DC'    :   DC,
                    'CS'    :   CS,
                    'RST'   :   RST,
                    'LED'   :   BL, #backlight   
    }
    global lcd_1

    lcd_1   =   LCD(PIN_OUT)
    lcd_1.set_backlight(ON)
    lcd_1.set_font(font_vi)

def menubar_init():

    global debug_bar
    # from MenuBarView    import View
    # view    =   View(lcd = lcd_1, font = font_vi, padding = 0)

    debug_bar   =   MenuBar( item_name          =   'MENU',
                             font               =   font_vi, 
                             padding            =   0, 
                             default_lcd        =   lcd_1, 
                             default_buttons    =   buttons)

    debug_bar.add_item('Gọi món')
    debug_bar.add_item('Xác nhận order')
    debug_bar.add_item('Xem hoá đơn')
    debug_bar.add_item('Xuất QR')
    debug_bar.add_item('Thoát')

    debug_bar.set_table_id(TABLE_ID)

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    mqtt_table.subscribe(TOPIC_NEW_MENU)
    mqtt_table.subscribe(TOPIC_ITEMS_RECEIVED)
    mqtt_table.subscribe(TOPIC_RECEIPT)

def on_message(client, userdata, message):
    if (message.topic   ==  TOPIC_NEW_MENU):
        menu_data       =   json.loads(message.payload.decode('utf-8'))
        write_new_menu(menu_data)

    elif (message.topic ==  TOPIC_ITEMS_RECEIVED):
        received_item   =   json.loads(message.payload.decode('utf-8'))
        write_receipt(received_item)

        # remove received items from order list, mark them as "not being queued anymore"
        order_list_remove(received_item)

    elif (message.topic ==  TOPIC_RECEIPT):
        check_out(log = message.payload.decode('utf-8'))

def check_out(log):
    # after checking out, clear and save this session's receipt and start new session
    from time import localtime, strftime
    import codecs

    # store this sessions receipt as new log
    file_name   =   strftime("%d.%m.%Y_%H.%M.log", localtime())

    with codecs.open('./logs/{}'.format(file_name), 'w', encoding = 'utf-8') as f:
        f.write(log)

    # clear old session receipt and logs
    with open('logs/order_list.json', 'w') as f:
        f.write('[]')

    with open('logs/receipt.json', 'w') as f:
        f.write('[]')

def write_receipt(received_item):
    with open('logs/receipt.json') as receipt:
        item_list   =   json.load(receipt)

    # append new received item to receipt
    item_list.append(received_item)

    with open('logs/receipt.json', 'w') as receipt:
        receipt.write(json.dumps(item_list))
    
def write_new_menu(menu_data):
    with codecs.open('data/menu.json', 'w', encoding = 'utf-8') as f:
        f.write(json.dumps(menu_data, ensure_ascii = False, indent = 2))

    debug_bar.has_new_menu = True

def order_list_remove(item):
    order_list      =   debug_bar.log_read()

    item_id         =   item['item_id']
    target_index    =   ""

    print(order_list)
    for index, order in enumerate(order_list):
        if (order['item_id'] == item_id):
            target_index    =   index
    
    # remove received item from waiting list    
    order_list.pop(target_index)

    with codecs.open('logs/order_list.json', 'w', encoding = 'utf-8') as order_log:
        order_log.write(json.dumps(order_list, ensure_ascii = False, indent = 2))


def mqtt_init():
    global mqtt_table

    mqtt_table   =   paho.mqtt.client.Client()

    mqtt_table.on_connect    =   on_connect
    mqtt_table.on_message    =   on_message

    mqtt_table.connect(SERVER_ADDRESS, 1883)

    thread_get_menu =   threading.Thread(target = mqtt_table.loop_forever, args = (), daemon = True)
    thread_get_menu.start()

button_init()
lcd_init()
menubar_init()

try:
    mqtt_init()
except ConnectionRefusedError: 
    import sys
    print('Failed to connect. Have you started the mqtt server?')
    sys.exit()

debug_bar.update_menu()

# delete logs from last session
try:
    with open('logs/order_list.json', 'w') as f:
        f.write('[]')
except FileNotFoundError:
    import os
    # if no logs folder, force mkdir
    os.system('mkdir -p logs')

    with open('logs/order_list.json', 'w') as f:
        f.write('\{\}')

# delete receipt buffer from last session
with open('logs/receipt.json', 'w') as f:
    f.write('[]')

# delete waiting list from last session
with open('logs/waiting_list.json', 'w') as f:
    f.write('[]')

debug_bar.start()