from    pcd8544.font.font_vi    import  font_vi
from    pcd8544.lcd             import  LCD_HEIGHT

class View:
    def __init__(self, lcd, font = font_vi, padding = 0):
        self.__lcd          =   lcd
        self.__font         =   font
        self.__padding      =   padding

        self.__title_height =   self.__font['HEIGHT'] + padding
        self.__font_height  =   self.__font['HEIGHT']

    def show(self, item_name, item_index, window_position, lcd = None):
        lcd     =   lcd or self.__lcd

        '''
        a pcd8544 has a field of view about 2 - 3 items,
        which means it can show that much items at the same time,
        this variable hold the position of current drawing item,
        for example 0 means it's drawing at the first line out of a total
        of 3 items, or 2 means it's drawing the last item and soon 
        the window position will have to scroll down
        '''
        position_in_window  =   item_index - window_position

        y       =   (position_in_window * self.__font_height) + (self.__title_height + 2) + (self.__padding * item_index)

        # special case  to make it look better when using vietnamese font
        if (self.__font == font_vi) and (position_in_window > 0):
            y += 3

        if (y + self.__font_height) > LCD_HEIGHT:
            return

        lcd.put_string(item_name, x = 2, y = y)



    def draw_title(self, title, lcd = None, font = None):
        lcd     =   lcd     or self.__lcd
        font    =   font    or self.__font

        lcd.draw_rect(0, 0, 83, 47)
        lcd.put_string(title, y = self.__padding, is_center = True, font = self.__font)
        lcd.invert_rect(1, 1, 82, self.__title_height)
